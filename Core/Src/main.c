//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-�����
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stdlib.h>
#include <dispcolor.h>
#include <gpio.h>
#include <rtc.h>
#include <clock.h>
#include <compas.h>
#include <clock.h>
#include <compas.h>
#include <textfading.h>
#include <thermostat.h>
#include "main.h"


void SystemClock_Config(void);


// ���, � �������� ���������� ������
#define Button_Port		GPIOA
#define Button_Pin 		(1 << 0)
uint8_t Button_OldState;

// ������ ������
#define ModesNum	15

#define ModeBlack	0
#define ModeRed		1
#define ModeGreen	2
#define ModeBlue	3
#define ModeWhite	4
#define ModeText	5
#define ModeClock1	6
#define ModeClock2	7
#define ModeClock3	8
#define ModeClock4	9
#define ModeCompas1	10	// ������
#define ModeCompas2	11	// ������
#define ModeHTP		12	// �����������-���������-��������
#define ModeTherm1	13	// ��������� ������� ����
#define ModeTherm2	14	// ��������� ������� ����
#define ModeHvac	15	// �����������
#define ModeHvac2	16	// ��� ������ � ���������� ��

// ������� ����� ������
static uint8_t Mode = ModeBlack;

// ��������� ������������ �������� ������ �� ���������
void SwitchMode() {
	if (++Mode >= ModesNum)
		Mode = 0;
}

int main(void) {
	HAL_Init();
	SystemClock_Config();

	gpio_SetGPIOmode_In(Button_Port, Button_Pin, gpio_PullUp);

	RTC_Init();

	// ��������� ����� ��� �����
	RTC_TimeTypeDef timeNow;
	timeNow.Hours = 19;
	timeNow.Minutes = 23;
	timeNow.Seconds = 45;
	RTC_SetTime(&timeNow);

	HAL_Delay(1);
	RTC_GetTime(&timeNow);

	// ������������� ������������
	srand(timeNow.SubSeconds);

	// ������������� �������
	dispcolor_Init(240, 240);

	while (1) {
		// ���� ���� ������ ������ - ������������� �� ��������� �����
		uint8_t ButtonState = HAL_GPIO_ReadPin(Button_Port, Button_Pin);
		if (!ButtonState && Button_OldState) {
			SwitchMode();
		}
		Button_OldState = ButtonState;

		switch (Mode) {
		case ModeBlack:
			dispcolor_FillScreen(BLACK);
			dispcolor_Update();
			HAL_Delay(100);
			break;
		case ModeRed:
			dispcolor_FillScreen(RED);
			dispcolor_Update();
			HAL_Delay(100);
			break;
		case ModeGreen:
			dispcolor_FillScreen(GREEN);
			dispcolor_Update();
			HAL_Delay(100);
			break;
		case ModeBlue:
			dispcolor_FillScreen(BLUE);
			dispcolor_Update();
			HAL_Delay(100);
			break;
		case ModeWhite:
			dispcolor_FillScreen(WHITE);
			dispcolor_Update();
			HAL_Delay(100);
			break;
		case ModeClock1:
			RTC_GetTime(&timeNow);
			DrawClock(timeNow.Hours, timeNow.Minutes, timeNow.Seconds, 0, 0);
			break;
		case ModeClock2:
			RTC_GetTime(&timeNow);
			DrawClock(timeNow.Hours, timeNow.Minutes, timeNow.Seconds, 0, 1);
			break;
		case ModeClock3:
			RTC_GetTime(&timeNow);
			DrawClock(timeNow.Hours, timeNow.Minutes, timeNow.Seconds, 1, 0);
			break;
		case ModeClock4:
			RTC_GetTime(&timeNow);
			DrawClock(timeNow.Hours, timeNow.Minutes, timeNow.Seconds, 1, 1);
			break;
		case ModeCompas1:	// ������
			DrawCompas(0);
			break;
		case ModeCompas2:	// ������
			DrawCompas(1);
			break;
		case ModeHTP:		// �����������-���������-��������
			break;
		case ModeTherm1:	// ��������� ������� ����
			Test_Therm(0);
			break;
		case ModeTherm2:	// ��������� ������� ����
			Test_Therm(1);
			break;
		case ModeHvac:	// �����������
			break;
		case ModeHvac2:	// ��� ������ � ���������� ��
			break;
		case ModeText:
			Test_TextFading("����������� � ���������", 25, 110);
			SwitchMode();
			break;
		}
	}
}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {
	RCC_OscInitTypeDef RCC_OscInitStruct = { 0 };
	RCC_ClkInitTypeDef RCC_ClkInitStruct = { 0 };
	RCC_PeriphCLKInitTypeDef PeriphClkInitStruct = { 0 };

	// Configure the main internal regulator output voltage
	__HAL_RCC_PWR_CLK_ENABLE();
	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE1);

	// Initializes the RCC Oscillators according to the specified parameters in the RCC_OscInitTypeDef structure.
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSE | RCC_OSCILLATORTYPE_LSE;
	RCC_OscInitStruct.HSEState = RCC_HSE_ON;
	RCC_OscInitStruct.LSEState = RCC_LSE_ON;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
	RCC_OscInitStruct.PLL.PLLM = 25;
	RCC_OscInitStruct.PLL.PLLN = 200;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 4;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		Error_Handler();
	}
	// Initializes the CPU, AHB and APB buses clocks
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK	| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_3) != HAL_OK) {
		Error_Handler();
	}
	PeriphClkInitStruct.PeriphClockSelection = RCC_PERIPHCLK_RTC;
	PeriphClkInitStruct.RTCClockSelection = RCC_RTCCLKSOURCE_LSE;
	if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInitStruct) != HAL_OK) {
		Error_Handler();
	}
}


/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @retval None
 */
void Error_Handler(void) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */

	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
