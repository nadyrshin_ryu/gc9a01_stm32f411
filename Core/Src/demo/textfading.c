//------------------------------------------------------------------------------
// This is Open source software. You can place this code on your site, but don't
// forget a link to my YouTube-channel: https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// ��� ����������� ����������� ���������������� ��������. �� ������ ���������
// ��� �� ����� �����, �� �� �������� ������� ������ �� ��� YouTube-�����
// "����������� � ���������" https://www.youtube.com/channel/UChButpZaL5kUUl_zTyIDFkQ
// �����: �������� ������ / Nadyrshin Ruslan
//------------------------------------------------------------------------------
#include <stm32f4xx_hal.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <dispcolor.h>
#include <font.h>
#include "textfading.h"

static char Str[32];

void Test_TextFading(char *pStr, int16_t X, int16_t Y) {
	uint8_t Len = strlen(pStr);

	dispcolor_ClearScreen();
	dispcolor_Update();
	HAL_Delay(20);

	dispcolor_SetBrightness(100);

	for (uint8_t i = 0; i < Len; i++) {
		memcpy(Str, pStr, i + 1);
		Str[i + 1] = 0;
		dispcolor_ClearScreen();
		dispcolor_printf(X, Y, FONTID_16F, RGB565(255, 255, 255), Str);
		dispcolor_Update();
		HAL_Delay(10);
	}

	HAL_Delay(1000);

	for (uint8_t i = 0; i <= 100; i++) {
		dispcolor_SetBrightness(100 - i);
		HAL_Delay(15);
	}

	dispcolor_ClearScreen();
	dispcolor_Update();
	HAL_Delay(500);

	for (uint8_t i = 0; i <= 100; i++) {
		dispcolor_SetBrightness(i);
		HAL_Delay(5);
	}
}
